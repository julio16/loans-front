import { extendObservable } from 'mobx'
import { postNewClient, postLogin } from '../helper/loans-api'
import jwt from 'jsonwebtoken'
import LoanModel from './LoanModel'

class UserModelMobx {
  constructor () {
    extendObservable(this, {
      viewModal: false,
      newClientData: {},
      loggedUserData: {}
    })
  }

  doLogin (data) {
    return postLogin(data)
      .then(response => {
        this.loggedUserData.jwt = response
        this.loggedUserData.email = jwt.decode(response).email
      })
  }

  newClient() {
    alert(`TODO Validate Form Params ${JSON.stringify(this.newClientData)}`)
    postNewClient(this.newClientData)
      .then(() => this.doLogin({username: this.newClientData.email, password: this.newClientData.password}))
      .then(() => LoanModel.applyLoan())
      .then(() => LoanModel.confirmLoan())
      .finally(() => this.viewModal = false)
  }

  changeModel (e) {
    this.newClientData[e.target.name] = e.target.value
  } 

  showModal () {
    this.viewModal = true
  }

  hideModal () {
    this.viewModal = false
  }
}

var UserModel = new UserModelMobx()
export default UserModel