import { extendObservable } from 'mobx'
import { getLoanConstrains, getLoanOffer, getClientLoans, postApplyLoan, putConfirmLoan } from '../helper/loans-api'

class LoanModelMobx {
  constructor () {
    extendObservable(this, {
      loanConstrains: {},
      requestLoanData: {},
      loanSimulationData: {},
      loanUserList: []
    })
  }

  async getLoansList () {
    let loanList = await getClientLoans()
    this.loanUserList = loanList._embedded.loans
  }

  async loadLoanConstraints ()  {
    let loanConstrains = await getLoanConstrains()
    this.loanConstrains = {...loanConstrains}
    this.requestLoanData.amount = loanConstrains.amountInterval.defaultValue
    this.requestLoanData.term = loanConstrains.termInterval.defaultValue
    await this.simulateLoan()
  }

  async simulateLoan () {
    let loadOffer = await getLoanOffer(this.requestLoanData)
    this.loanSimulationData = {...loadOffer}
  }

  async applyLoan () {
    await postApplyLoan(this.requestLoanData)
  }

  async confirmLoan () {
    await putConfirmLoan()
    alert('Loan is confirmed, Go to History')
  }

  changeLoanAmount(newValue) {
    this.requestLoanData.amount = newValue
  }

  changeLoanTerm(newValue) {
    this.requestLoanData.term = newValue
  }

  async newLoan () {
    await LoanModel.applyLoan()
    await LoanModel.confirmLoan()
  }

}

var LoanModel = new LoanModelMobx()
export default LoanModel