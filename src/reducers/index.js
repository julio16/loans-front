import { combineReducers } from 'redux'


const loanConstraints = (state = {}, action) => {
  switch (action.type) {
    case 'CONSTRAINTS':
      return action.loanConstraints
    default:
      return state
  }
}

const requestLoanData = (state = {}, action) => {
  switch (action.type) {
    case 'LOADVALUES':
      return action.requestLoanData
    default:
      return state
  }
}

const loanSimulationData = (state = {}, action) => {
  switch (action.type) {
    case 'SIMULATION':
      return action.simulateData
    default:
      return state
  }
}

const rootReducer = combineReducers({
  loanConstraints,
  requestLoanData,
  loanSimulationData
})

export default rootReducer
