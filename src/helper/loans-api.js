import RSVP from 'rsvp'
import axios from 'axios'
import UserModel from '../models/UserModel'
import _ from 'lodash'

const config = {
  apiBaseUrl: 'http://localhost:3000'
}

function generalRequest (axiosPromise, rsvpResolve, rspvReject) {
  axiosPromise.then(response => {
    //Here you can do general actions when return the request like reject the promise if status code != 2XX
    rsvpResolve(response.data)
  }).catch(error => {
    alert(`Error code ${error.response.status} - Error Text ${error.response.statusText} - Error Description ${JSON.stringify(error.response.data)}`)
    //Here you can normalize your request errors
    rspvReject(error)
  })
}

function getHeaders () {
  let jwtToken = UserModel.loggedUserData.jwt
  let headers = {}
  if (jwtToken) {
    _.extend(headers, {Authorization: `bearer ${jwtToken}`})
  }
  return {
    headers: headers
  }
}

function doPost (url, data) {
  return new RSVP.Promise(function (resolve, reject) {
    //You can do general actions for the post request (like add headers)
    let axiosPromise = axios.post(url, data, getHeaders())
    generalRequest(axiosPromise, resolve, reject)
  })
}

function doGet (url) {
  return new RSVP.Promise(function (resolve, reject) {
    //You can do general actions for the get request (like add headers)
    let axiosPromise = axios.get(url, getHeaders())
    generalRequest(axiosPromise, resolve, reject)
  })
}

function doPut (url, data) {
  return new RSVP.Promise(function (resolve, reject) {
    let axiosPromise = axios.put(url, data, getHeaders())
    generalRequest(axiosPromise, resolve, reject)
  })
}

function postNewClient (data) {
  const url = `${config.apiBaseUrl}/clients`
  return doPost(url, data)
}

function postLogin (data) {
  const url = `${config.apiBaseUrl}/login`
  return doPost(url, data)
}

function getLoanConstrains () {
  const url = `${config.apiBaseUrl}/application/constraints`
  return doGet(url)
}

function getLoanOffer (data) {
  const url = `${config.apiBaseUrl}/application/offer?amount=${data.amount}&term=${data.term}`
  return doGet(url)
}

function getClientLoans (data) {
  const url = `${config.apiBaseUrl}/clients/loans`
  return doGet(url)
}

function postApplyLoan (data) {
  const url = `${config.apiBaseUrl}/clients/application?amount=${data.amount}&term=${data.term}`
  return doPost(url, data)
}

function putConfirmLoan (data) {
  const url = `${config.apiBaseUrl}/clients/application`
  return doPut(url, data)
}

export {
  getLoanConstrains,
  getLoanOffer,
  postNewClient,
  postLogin,
  getClientLoans,
  postApplyLoan,
  putConfirmLoan
}