import React, { Component } from 'react'
import { Navbar } from 'react-bootstrap'
import NewUserModel from '../models/UserModel'
import { observer } from 'mobx-react'
import { withRouter}  from 'react-router-dom'

class Header extends Component {
  get renderLoggedData () {
    return NewUserModel.loggedUserData.email
      ? <p>Logged User: {NewUserModel.loggedUserData.email}</p>
      : ''
  }
  
  render() {
    return (
      <Navbar bg="dark" expand="lg">
        <Navbar.Brand>LOANS FRONT HEADER</Navbar.Brand>
        {this.renderLoggedData}
      </Navbar>
    )
  }
}
export default withRouter(observer(Header))