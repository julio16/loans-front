import React, { Component } from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import NewUserModel from '../models/UserModel'
import { observer } from 'mobx-react'

class NewUser extends Component {
  render() {
    return (
      <Modal show={NewUserModel.viewModal} onHide={() => NewUserModel.hideModal()}>
        <Modal.Header closeButton>
          <Modal.Title>New User Form</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
          <Form.Row>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control name="name" type="text" placeholder="Enter name" onChange={e => NewUserModel.changeModel(e)}/>
            </Form.Group>
            <Form.Group>
              <Form.Label>Surname</Form.Label>
              <Form.Control name="surname" type="text" placeholder="Enter surname"  onChange={e => NewUserModel.changeModel(e)}/>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control name="email" type="email" placeholder="Enter email"  onChange={e => NewUserModel.changeModel(e)}/>
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control name="password" type="password" placeholder="Enter password"  onChange={e => NewUserModel.changeModel(e)}/>
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group>
              <Form.Label>Personal Id</Form.Label>
              <Form.Control name="personalId" type="text" placeholder="Enter personal id (11 digits)"  onChange={e => NewUserModel.changeModel(e)}/>
            </Form.Group>
          </Form.Row>
        </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" size="lg" active onClick={() => NewUserModel.newClient()}>
            Get loan
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}
export default observer(NewUser)