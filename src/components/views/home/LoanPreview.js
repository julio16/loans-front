import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card } from 'react-bootstrap'
import { tGet } from '../../../helper/transverser'

class LoanPreview extends Component {
  render() {
    return (
      <Card>
        <Card.Body>
          <h3>Loan Amount</h3>
          <div>{tGet('loanSimulationData.principalAmount')(this.props)}</div>
          <h3>Interes Amount</h3>
          <div>{tGet('loanSimulationData.interestAmount')(this.props)}</div>
          <h3>Total Amount</h3>
          <div>{tGet('loanSimulationData.totalAmount')(this.props)}</div>
          <h3>Due Date</h3>
          <div>{tGet('loanSimulationData.dueDate')(this.props)}</div>
        </Card.Body>
      </Card>
    )
  }
}

const mapStateToProps = state => {

  const { loanSimulationData } = state

  return {
    loanSimulationData
  }
}

export default connect(mapStateToProps)(LoanPreview)