import React, { Component } from 'react'
import { Card } from 'react-bootstrap'
import { loadLoanConstraints, loadInitialData, simulateLoan, changeLoanValue  } from '../../../actions'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'
import { tGet } from '../../../helper/transverser'
import { connect } from 'react-redux'

class LoanControl extends Component {
  
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(loadLoanConstraints())
      .then(state => dispatch(loadInitialData(state.loanConstraints)))
      .then(state => dispatch(simulateLoan(state.requestLoanData)))
  }

  handleSliderChange (loanProp, newValue) {
    const { requestLoanData, dispatch} = this.props
    dispatch(changeLoanValue(requestLoanData, loanProp, newValue))
  }

  handleCompleteSlider () {
    const { requestLoanData, dispatch} = this.props
    dispatch(simulateLoan(requestLoanData))
  }

  render() {
    return (
      <Card>
        <Card.Body>
          <h3>Loan Amount</h3>
          <Slider
            min={tGet('loanConstraints.amountInterval.min')(this.props)}
            max={tGet('loanConstraints.amountInterval.max')(this.props)}
            step={tGet('loanConstraints.amountInterval.step')(this.props)}
            value={tGet('requestLoanData.amount')(this.props)}
            onChange={value => this.handleSliderChange('amount', value)}
            onChangeComplete={this.handleCompleteSlider.bind(this)}
          />
          <div className='value'>{tGet('requestLoanData.amount')(this.props)}</div>
          <h3>Loan Term (Years)</h3>
          <Slider
            min={tGet('loanConstraints.termInterval.min')(this.props)}
            max={tGet('loanConstraints.termInterval.max')(this.props)}
            value={tGet('requestLoanData.term')(this.props)}
            onChange={value => this.handleSliderChange('term', value)}
            onChangeComplete={this.handleCompleteSlider.bind(this)}
          />
          <div className='value'>{tGet('requestLoanData.term')(this.props)}</div>
        </Card.Body>
      </Card>
    )
  }
}

const mapStateToProps = state => {

  const { loanConstraints, requestLoanData } = state

  return {
    loanConstraints,
    requestLoanData
  }
}

export default connect(mapStateToProps)(LoanControl)