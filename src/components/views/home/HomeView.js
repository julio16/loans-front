import React, { Component } from 'react'
import LoanPreview from './LoanPreview'
import LoanControl from './LoanControl'
import './HomeView.scss'
import NewUserModel from '../../../models/UserModel'
import UserModel from '../../../models/UserModel'
import LoanModel from '../../../models/LoanModel'

class HomeView extends Component {

  async applyLoan () {
    if(UserModel.loggedUserData.jwt) {
      await LoanModel.newLoan()
    } else {
      NewUserModel.showModal()
    }
  }

  render() {
    return (
      <div className="home-view">
        <div className="home-view__container">
          <LoanPreview/>
          <LoanControl/>
        </div>
      </div>
    )
  }
}
export default HomeView