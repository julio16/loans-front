import React, { Component } from 'react'
import { Col, Row } from 'react-bootstrap'

class HistoryRow extends Component {

  render() {
    return (
      <Row className="history-row">
        <Col>Principal Amount {this.props.loan.principalAmount}</Col>
        <Col>Interest Amount {this.props.loan.interestAmount}</Col>
        <Col>Total Amount {this.props.loan.totalAmount}</Col>
        <Col>Status {this.props.loan.status}</Col>
      </Row>
    )
  }
}
export default HistoryRow