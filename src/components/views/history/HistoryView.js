import React, { Component } from 'react'
import './HistoryView.scss'
import { observer } from 'mobx-react'
import LoanModel from '../../../models/LoanModel'
import HistoryRow from './HistoryRow'

class HistoryView extends Component {

  async componentDidMount () {
    await LoanModel.getLoansList()
  }

  render() {
    return (
      <div className="history-view">
        <div className="history-view__container">
          {LoanModel.loanUserList.map((loan, loanIndex) => <HistoryRow key={`loan_${loanIndex}`} loan={loan}/>)}
        </div>
      </div>
    )
  }
}
export default observer(HistoryView)