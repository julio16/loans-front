import 'bootstrap/dist/css/bootstrap.min.css'
import React from 'react'
import './App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom"
import Header from './components/Header'
import HomeView from './components/views/home/HomeView'
import HistoryView from './components/views/history/HistoryView'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import reducer from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension'

function App() {

  const middleware = [ thunk ]
  if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger())
  }

  const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(...middleware))
  )

  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Header/>
          <Switch>
            <Route path="/history">
              <HistoryView/>
            </Route>
            <Route path="/">
              <HomeView />
            </Route>
          </Switch> 
        </Router>
      </Provider>
    </div>
  )
}

export default App
