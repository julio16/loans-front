import { getLoanConstrains, getLoanOffer } from '../helper/loans-api'

export const receiveConstraints = responseData => {
  return ({
    type: 'CONSTRAINTS',
    loanConstraints: responseData
  })
}

export const loadLoanConstraints = () => dispatch => {
  return getLoanConstrains()
    .then(response => dispatch(receiveConstraints(response)))
}

export const loadInitialData = (loanConstraints) => dispatch => {
  return dispatch({
    type: 'LOADVALUES',
    requestLoanData: {
      amount: loanConstraints.amountInterval.defaultValue,
      term: loanConstraints.termInterval.defaultValue
    }
  })
}

export const changeLoanValue = (requestLoanData, loanProp, newValue ) => dispatch => {
  let dispatchData = {...requestLoanData, [loanProp]: newValue}
  return dispatch({
    type: 'LOADVALUES',
    requestLoanData: dispatchData
  })
}

export const simulateLoan = (requestLoanData) => dispatch => {
  return getLoanOffer(requestLoanData)
    .then(responseData => dispatch({
      type: 'SIMULATION',
      simulateData: responseData
    }))
}
